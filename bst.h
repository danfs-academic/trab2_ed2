#ifndef ABB_H_
#define ABB_H_

#include "item.h"

//* Cabeçalho da Árvore Binária de Busca
//* A Árvore será organizada considerando o espaço livre dos discos
typedef struct node Bst;

//* Inicializa a Binary Search Tree
void create_bst();

//* Insere um elemento 'tam' na árvore
//  Atualiza o conteúdo de algum nó, se possível, se não, cria um nó novo
void insert_bst(Item tam);

//* Libera a memória da BST
void free_bst();

//* Retorna a quantidade de nós existentes na BST
int size_bst();

#endif