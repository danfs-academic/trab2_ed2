FLAG = -g -Wall

all: trab2 clear

trab2: main.o
	gcc -o trab2 *.o $(FLAG)

main.o: main.c fit.o
	gcc -c main.c $(FLAG)

fit.o: fit.c pq.o bst.o
	gcc -c fit.c $(FLAG)

pq.o: pq.c
	gcc -c pq.c $(FLAG)

bst.o: bst.c
	gcc -c bst.c $(FLAG)

clear:
	rm *.o

run: all
	./trab2 in/100.txt
	./trab2 in/1000.txt
	./trab2 in/10000.txt
	./trab2 in/100000.txt	