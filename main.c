#include <stdio.h>
#include <stdlib.h>
#include "fit.h"

int main(int argc, char const *argv[]) {
    FILE* in = fopen(argv[1], "r");

    printf("%d\n", worst_fit(in));
    printf("%d\n", best_fit(in));
    printf("%d\n", worst_fit_ord(in));
    printf("%d\n", best_fit_ord(in));

    printf("\n");
    fclose(in);

    return 0;
}
