#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

//* Structs
struct node {
    Item info;          //Informação do nó
    Bst *left, *right;  //Filhos do nó
};

//* Variáveis estáticas
static Bst* head;           //O topo (cabeça) da BST
static int qtdd;            //A qtdd de nós da árvore
static Bst* removed_node;   //O último nó removido pela função static remove_node

//* Funções estáticas

//* Cria um nó novo
static Bst* create_node(Item tam) {
    Bst* node = malloc(sizeof(Bst));
    
    node->left = node->right = NULL;//Os filhos de um nó novo são
    node->info = 1000000 - tam;     //O espaço livre é 10^6 subtraido do tamanho do arquivo inserido
    
    qtdd++;

    return node;
}

/*  
* Encontra o nó aonde se deve inserir o conteúdo 'tam'.
* 'raiz' é o nó atual de procura, 'atual' é o melhor nó válido encontrado até então
*/
static Bst* find_node(Bst* raiz, Item tam, Bst* atual) {
    //Se chegamos numa raiz nula, o melhor nó é o último encontrado
    if(raiz == NULL) return atual;
    //Se encontrarmos um nó com o tamanho exato, retornar esse nó
    if(raiz->info == tam) return raiz;

    //Confere o conteúdo do nó pra saber para qual lado da sub-arvore continuar a procura
    if(isGreater(raiz->info, tam)) {
        //Se a raiz for maior que 'tam', 'raiz' é melhor que 'atual'
        return find_node(raiz->left, tam, raiz);
    } else {
        //Se a raiz for menor que 'tam', o melhor continua sendo 'atual'
        return find_node(raiz->right, tam, atual);
    }
}

//* Função para adicionar um elemento 'ins' numa binary search tree 'raiz'
static Bst* insert_node(Bst* raiz, Bst* ins) {
    //Se estiver tentando inserir numa sub-arvore nula, o nó no lugar
    if(raiz == NULL)                            raiz = ins;
    //Se o tamanho da nova for maior que a raiz, inserir a direita
    else if(isGreater(ins->info, raiz->info))   raiz->right = insert_node(raiz->right, ins);
    //Caso contrário, inserir a esquerda
    else                                        raiz->left = insert_node(raiz->left, ins);
    
    //Retornar a raiz
    return raiz;
}

//* Remove um nó (que se sabe o ponteiro) da sub-árvore 'raiz' e retornar a sub árvore resultante
static Bst* remove_node(Bst* raiz, Bst* node) {
    //Se chegar numa subárvore nula, não tem o que retirar
    if(raiz == NULL)    return NULL;
    //Chegamos no nó que deve ser retirado
    if(raiz == node) {
        //Salvar o endereço do nó que será removido, para ser usado posteriormente
        removed_node = node;
        if(raiz->left == NULL && raiz->right == NULL)   return NULL;        //A sub-árvore resultante será nula
        else if(raiz->left  == NULL)                    raiz = node->right; //A sub-árvore resultante será o filho da direita
        else if(raiz->right == NULL)                    raiz = node->left;  //A sub-árvore resultante será o filho da esquerda
        //Caso o nó a ser revomido tenha dois filhos
        else {
            //Cria uma variável que é iniciada no filho da esquerda, e será atualizada até alcançar o nó mais a direita a partir daí
            Bst* aux = raiz->left;
            while(aux->right != NULL) aux = aux->right;
            //Troca as chaves do nó a ser removido e a de aux(que é o que realmente será removido)
            exch(aux->info, node->info);
            //Refaz o algoritmo, para remover 'aux' da sub-árvore a esquerda
            //(esse processo irá re-atualizar a variavel "removed_node" para o ponteiro correto antes de sair dessa função)
            raiz->left = remove_node(raiz->left, aux);
        }
        removed_node->right = removed_node->left = NULL;    //Anular os filhos do nó removido
    }

    else if(isGreater(node->info, raiz->info))  raiz->right = remove_node(raiz->right, node);
    else                                        raiz->left  = remove_node(raiz->left, node);

    return raiz;
}

//* Função usada recursivamente para liberar a BST
static Bst* liberar(Bst* node) {
    if(node == NULL) return NULL;

    //Liberar primeiro os filhos, e então o nó em si
    node->left = liberar(node->left);
    node->right = liberar(node->right);
    free(node);

    return NULL;
}

//* Funções públicas
void create_bst() {
    head = NULL;    //O topo dá BST é NULL
    qtdd = 0;       //A qtdd inicial de nós é 0
}

void insert_bst(Item tam) {
    //Se o tamanho do arquivo for 0, ignorar o tentativa de inserção, pois é irrelevante
    if(tam <= 0) return;
    //Procura um nó de inserção apenas se 'tam' for menor que 1000000 (se for maior ou igual, não existirá nó que caiba)
    Bst* ins = NULL;
    if(tam < 1000000) ins = find_node(head, tam, NULL);

    //Se não existe nó que caiba o arquivo, criar um novo e inserir na árvore
    if(ins == NULL) {
        ins = create_node(tam);
        head = insert_node(head, ins);
    } else {
        //Se não, remover o nó, atualizar seu tamanho por 'tam' e reinserí-lo
        head = remove_node(head, ins);
        removed_node->info -= tam;
        head = insert_node(head, removed_node);
    }
}

void free_bst() {
    head = liberar(head);   //Chama a função estática de liberação no topo
    qtdd = 0;               //Zera o tamanho da árvore
}

int size_bst() {
    return qtdd;
}