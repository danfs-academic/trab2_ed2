#ifndef PQ_H_
#define PQ_H_

#include "item.h"
#include <stdbool.h>

typedef struct pq PQ;

//* Inicializa a fila (estaticamente) com tamanho máximo 'tam'
void init_pq(int tam);

//* Insere o elemento 'i' na fila
void insert_pq(Item i);

//* Remove, e retorna, o maior elemento da fila
Item del_max();

//* Retorna o tamanho da fila
int size_pq();

//* Diz se a fila está vazia
bool is_empty_pq();

//* Decrementa a chave do maior elemento pela chave de 'tam'
//  Obs.: Se a chave do maior elemento ficar negativa, ao invés disso inserirá 'tam' isolado
void decres_chave_max(Item tam);

//* Libera o espaço alocado pela fila com prioridade
void free_pq();

#endif