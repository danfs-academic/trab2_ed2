#ifndef FIT_H_
#define FIT_H_

#include "pq.h"
#include "bst.h"
/*
 ! AVISO: nenhuma dessas funções fecha o arquivo que recebe de entrada!
 !        Isso deve ser feito exteriormente (na main, por exemplo)

 * Todas as funções tem as mesmas entradas, saídas e pré-condições:
 * Entrada: o ponteiro para um arquivo de entrada para a operação de fit
 * Saída:   o número de discos utilizados pela heurística especificada no nome
 * Pré:     o arquivo tem:  a quantidade de discos a serem lidos na primeira linha;
 *                          um disco por linha nas linhas seguintes.
*/
//* Heurística de Worst-Fit
int worst_fit(FILE* input);

//* Heurística de Worst-Fit com entrada ordenada
int worst_fit_ord(FILE* input);

//* Heurística de Best-Fit
int best_fit(FILE* input);

//* Heurística de Best-Fit com entrada ordenada
int best_fit_ord(FILE* input);

#endif // FIT_H_
