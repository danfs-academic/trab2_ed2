#include <stdio.h>
#include <stdlib.h>
#include "item.h"
#include "fit.h"

//* Função de comparação para o qsort
static int sortcomp(const void* p1, const void* p2) {
    return *(Item*)p2 - *(Item*)p1;
}

int worst_fit(FILE* in) {
    rewind(in); //Retorna o arquivo de entrada pro ponto inicial

    int qtdd;   //Armazena o tamanho máximo possível da fila
    fscanf(in, "%d\n", &qtdd);

    init_pq(qtdd);  //Inicializa a fila de prioridade

    Item n;
    //Lê todo o arquivo de entrada, e efetua o algoritmo
    for(int i = 0; i < qtdd; i++) {
        fscanf(in, "%d\n", &n);
        decres_chave_max(n);
    }
    //Reaproveita a variável pra armazenar o retorno
    qtdd = size_pq();
    //Libera a fila
    free_pq();

    return qtdd;
}

int worst_fit_ord(FILE* in) {
    rewind(in); //Retorna o arquivo de entrada pro ponto inicial
    
    int qtdd;   //Armazena o tamanho máximo possível da fila
    fscanf(in, "%d\n", &qtdd);

    init_pq(qtdd);  //Inicializa a fila de prioridade

    //Cria um vetor auxiliar que recebe as entradas, e será ordenado decrescentemente
    Item* aux = malloc(sizeof(Item)*qtdd);
    for(int i = 0; i < qtdd; i++) {
        fscanf(in, "%d\n", &aux[i]);
    }
    qsort(aux, qtdd, sizeof(Item), sortcomp);

    //Efetua o algoritmo lendo as entradas decrescentemente
    for(int i = 0; i < qtdd; i++) {
        decres_chave_max(aux[i]);
    }
    //Reaproveita a variável pra armazenar o retorno
    qtdd = size_pq();
    //Libera a fila e o vetor ordenado
    free(aux);
    free_pq();

    return qtdd;
}

int best_fit(FILE* in) {
    rewind(in); //Retorna o arquivo de entrada pro ponto inicial

    int qtdd;   //Armazena a quantidade de entradas
    fscanf(in, "%d\n", &qtdd);

    create_bst();   //Inicializa a bst

    Item n;
    //Lê todo o arquivo, inserindo os elementos na árvore
    for(int i = 0; i < qtdd; i++) {
        fscanf(in, "%d\n", &n);
        insert_bst(n);
    }
    //Reaproveita a variável pra guardar o retorno
    qtdd = size_bst();
    //Libera a árvore
    free_bst();

    return qtdd;
}

int best_fit_ord(FILE* in) {
    rewind(in); //Retorna o arquivo de entrada pro ponto inicial

    int qtdd;   //Armazena a quantidade de entradas
    fscanf(in, "%d\n", &qtdd);

    create_bst();   //Inicializa a bst

    //Cria um vetor para armazenar as entradas, e ordena-o decrescentemente
    Item* aux = malloc(sizeof(Item)*qtdd);
    for(int i = 0; i < qtdd; i++) {
        fscanf(in, "%d\n", &aux[i]);
    }
    qsort(aux, qtdd, sizeof(Item), sortcomp);

    //Efetua o algoritmo lendo as entradas decrescentemente
    for(int i = 0; i < qtdd; i++) {
        insert_bst(aux[i]);
    }
    //Reaproveita a variável pra guardar o retorno
    qtdd = size_bst();
    //Libera a árvore e o vetor de entradas
    free_bst();
    free(aux);

    return qtdd;
}