#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "pq.h"

//* Variáveis estáticas: vetor da heap; tamanho ocupado; e tamanho máximo da heap
static Item* heap;
static int tam;
static int max_tam;

//* Funções estáticas
static void bottom_up_heap(int pos) {
    //Enquanto não alcançar o topo da fila
    while (pos > 1) {
        //Se tiver que trocar com o pai, efetua a troca
        if( isGreater(heap[pos], heap[pos/2]) ) {
            exch(heap[pos], heap[pos/2]);
            pos = pos/2;
        } else {
            //Se não, quebra o ciclo
            break;
        }
    }
}

static void fix_down(int pos) {
    int l = pos*2;  //Filho da esquerda
    int r = l+1;    //Filho da direita
    int grt = pos;  //Posição do maior elemento entre o pai e seus filhos, iniciado como o pai

    //Para ambos os filhos: confere se eles existem
    //Se sim, e se forem maiores que 'grt', atualiza 'grt'
    if(l <= tam) if(isGreater(heap[l], heap[grt])) grt = l;
    if(r <= tam) if(isGreater(heap[r], heap[grt])) grt = r;

    //Caso a pos inicial não seja a posição do maior elem, efetuar a troca
    if(grt != pos) {
        exch(heap[grt], heap[pos]);
        fix_down(grt);              //Caso tenha ocorrido troca, é necessário repetir o ajuste
    }
}

static void aviso_fila_cheia(Item i) {
    printf("IMPOSSÍVEL INSERIR O ELEMENTO %d!! A FILA JÁ ESTÁ CHEIA!!\n", i);
}

//* Funções usáveis por outros arquivos
void init_pq(int qtdd) {
    heap = malloc(sizeof(Item)*(qtdd+1)); //tam+1 pois a posição 0 não é usada
    tam = 0;
    max_tam = qtdd;
}

void insert_pq(Item i) {
    //Se a fila já estiver cheia, ignorar a tentativa de inserção e printar aviso no terminal
    if(tam >= max_tam) aviso_fila_cheia(i);
    else {
        //Aumenta o tamanhoe e adiciona o elemento na primeira 'pos' livre
        int pos = ++tam;
        heap[pos] = i;
        //Conserta a posição do elemento recém adicionado
        bottom_up_heap(pos);
    }
}

Item del_max() {
    //Armazena o retorno
    Item max = heap[1];
    
    //Joga o último elemento (do vetor) no início, reduz o tamanho
    heap[1] = heap[tam];
    tam--;
    //Conserta a posição do elemento colocado na pos 1
    fix_down(1);
    //Retorna o item armazenado
    return max;
}

bool is_empty_pq() {
    return tam == 0;
}

int size_pq() {
    return tam;
}

void decres_chave_max(Item i) {
    //Se a fila estiver vazia, ou se 'i' não couber no maior elemento
    if (is_empty_pq() || i > heap[1]) {
        //Insere 'i' como elemento novo
        insert_pq(1000000-i);
    } else {
        //Se não, decrementa o maior elemento e conserta sua posição
        heap[1] -= i;
        fix_down(1);
    }
}

void free_pq() {
    free(heap);     //Libera a memória anterior
    heap = NULL;    //Anula o ponteiro
    tam = 0;        //Zera o tamanho atual
    max_tam = 0;    //Zera o tamanho máximo
}